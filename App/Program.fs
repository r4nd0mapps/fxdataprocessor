﻿module App

open Akka.Actor
open Akka.FSharp
open FSharp.Control.Reactive
open FSharpx
open FSharpx.Functional
open System
open System.IO

type TradeData = 
    { Id : int
      TimeStamp : DateTime
      BidPrice : float
      AskPrice : float }

type WindowedTradeData = 
    { CurrencyPair : string
      TimeStamp : DateTime
      AskPrice : float }

type DataFiles = 
    { Ref : string
      Trade : string }

type TradeDataRecordLoaderMessage = 
    | Load of DataFiles

type TradeDataRecordFilterMessage = 
    | Filter of string

type TradeDataRecordWindowerMessage = 
    | TradeData of string * TradeData

type TradeDataUIMessage = 
    | WindowedTradeData of WindowedTradeData

module Actors = 
    let tDataRecLoaderActorLoop (forwardTo : IActorRef) (mailbox : Actor<_>) = 
        let rec loop() = 
            actor { 
                let! message = mailbox.Receive()
                match message with
                | Load({ Ref = r; Trade = t }) -> 
                    printfn "Loader: Loading: ref = %s, trade = %s" r t
                    use fs = File.Open(t, FileMode.Open, FileAccess.Read, FileShare.Read)
                    use bs = new BufferedStream(fs)
                    use sr = new StreamReader(bs)
                    fun _ -> sr.ReadLine()
                    |> Seq.initInfinite
                    |> Seq.takeWhile ((<>) null)
                    |> Seq.iter (Filter >> forwardTo.Tell)
                return! loop()
            }
        loop()
    
    let tDataRecFilterActorLoop (forwardTo : IActorRef) (mailbox : Actor<_>) = 
        let rec loop() = 
            actor { 
                let! message = mailbox.Receive()
                match message with
                | Filter(r) -> 
                    let (>>=) = Option.op_GreaterGreaterEquals
                    let f = r.Split([| ',' |]).ToFSharpList()
                    
                    let id = 
                        f
                        |> List.tryItem 0
                        >>= Int32.parse
                    
                    let ts = 
                        f
                        |> List.tryItem 1
                        >>= DateTime.parseExact [|"yyyyMMdd hh:mm:ss.fff"|]
                    
                    let bp = 
                        f
                        |> List.tryItem 2
                        >>= Double.parse
                    
                    let ap = 
                        f
                        |> List.tryItem 3
                        >>= Double.parse
                    
                    let (<*>) = Option.op_LessMultiplyGreater
                    
                    let createTd i t b a = 
                        { Id = i
                          TimeStamp = t
                          BidPrice = b
                          AskPrice = a }
                    Option.returnM createTd <*> id <*> ts <*> bp <*> ap 
                    >>= (fun td -> if td.Id = 1 then Some ("AUD/USD", td) else None)
                    |> Option.fold (fun _ -> TradeData >> forwardTo.Tell) ()
                return! loop()
            }
        loop()
    
    let tDataWindowerActorLoop (forwardTo : IActorRef) (mailbox : Actor<_>) = 
        let ticks5Min = TimeSpan.FromMinutes(5.0).Ticks
        
        let rec loop (prevTd : TradeData option) = 
            actor { 
                let! message = mailbox.Receive()
                match message with
                | TradeData(id, td) -> 
                    match prevTd with
                    | Some prevTd -> 
                        if prevTd.TimeStamp.Ticks / ticks5Min + 1L = (td.TimeStamp.Ticks / ticks5Min) then 
                            { CurrencyPair = id
                              TimeStamp = DateTime(td.TimeStamp.Ticks / ticks5Min * ticks5Min)
                              AskPrice = (prevTd.AskPrice + td.AskPrice) / 2.0 }
                            |> WindowedTradeData
                            |> forwardTo.Tell
                            return! td
                                    |> Some
                                    |> loop
                        else 
                            return! td
                                    |> Some
                                    |> loop
                    | None -> 
                        return! td
                                |> Some
                                |> loop
            }
        loop None
    
    let tDataUIActorLoop (mailbox : Actor<_>) = 
        let rec loop() = 
            actor { 
                let! message = mailbox.Receive()
                match message with
                | WindowedTradeData(wtd) -> printfn "UI: rec = %A" wtd
                return! loop()
            }
        loop()

let system = ActorSystem.Create("FxDataProcessor")
let tDataUIActor = spawn system "TradeDataUI" <| Actors.tDataUIActorLoop
let tDataWindowerActor = spawn system "TradeDataRecordWindower" <| Actors.tDataWindowerActorLoop tDataUIActor
let tDataRecFilterActor = spawn system "TradeDataRecordFilter" <| Actors.tDataRecFilterActorLoop tDataWindowerActor
let tDataRecLoaderActor = spawn system "TradeDataRecordLoader" <| Actors.tDataRecLoaderActorLoop tDataRecFilterActor

(*
 - Terminate
 - Scale out strategy
 - Fault handling strategy
 - Logging
*)

[<EntryPoint>]
let main argv = 
    let rDataFile = @"D:\src\FxDataProcessor\reference-data.csv" // get from argv 
    let tDataFile = @"D:\src\FxDataProcessor\tick-data.csv" // get from argv
    tDataRecLoaderActor <! Load({ Ref = rDataFile
                                  Trade = tDataFile })
    system.WhenTerminated.Wait()
    0
