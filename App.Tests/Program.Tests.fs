﻿module App.Tests

open Akka.Actor
open Akka.FSharp
open Akka.TestKit.Xunit2
open System
open global.Xunit

let test (config : Akka.Configuration.Config) (fn : TestKit -> unit) = 
    use system = System.create "test-system" (config.WithFallback Akka.TestKit.Configs.TestConfigs.TestSchedulerConfig)
    use tk = new TestKit(system)
    fn tk

let testDefault t = test (Akka.TestKit.Configs.TestConfigs.TestSchedulerConfig) t

let spawnAsTestRef (tk : TestKit) (f : Actor<_> -> Cont<_, _>) : IActorRef = 
    let e = Linq.Expression.ToExpression(fun () -> new FunActor<_, _>(f))
    tk.ActorOfAsTestActorRef<FunActor<_, _>>(Props.Create e, tk.TestActor) :> IActorRef

let inline expectNoMsg (tk : TestKit) : unit = tk.ExpectNoMsg()

let expectMsg (tk : TestKit) (msg : 't) : 't option = 
    let reply = tk.ExpectMsg<'t>(msg, TimeSpan.FromSeconds(2.0) |> Nullable, "")
    if reply <> Unchecked.defaultof<'t> then Some reply
    else None

let (!!) = DateTime.Parse

let ``Data - Windower - All Tests`` : obj array seq = 
    [| [ (0.0, !!"00:05:00") ], None
       [ (0.0, !!"00:01:00")
         (1.0, !!"00:02:00") ], None
       [ (0.0, !!"00:14:59")
         (1.0, !!"00:15:00") ], Some(0.5, !!"00:15:00")
       [ (0.0, !!"00:14:59")
         (1.0, !!"00:15:01") ], Some(0.5, !!"00:15:00")
       [ (1.0, !!"01:47:44")
         (2.0, !!"01:48:04")
         (3.0, !!"01:54:01") ], Some(2.5, !!"01:50:00")
       [ (1.0, !!"01:47:44")
         (2.0, !!"01:50:04")
         (3.0, !!"01:54:01") ], Some(1.5, !!"01:50:00")
       [ (1.0, !!"01:47:44")
         (2.0, !!"00:50:04")
         (3.0, !!"01:54:01") ], None |]
    |> Seq.map (fun (a, b) -> 
           [| box a
              box b |])

[<Theory>]
[<MemberData("Data - Windower - All Tests")>]
let ``Windower - All Tests`` (apts : (float * DateTime) list, wapts : (float * DateTime) option) = 
    testDefault <| fun tk -> 
        let actor = spawnAsTestRef tk <| Actors.tDataWindowerActorLoop tk.TestActor
        apts |> List.iter (fun (ap, ts) -> 
                    { Id = 0 |> int
                      TimeStamp = ts
                      BidPrice = Double.NaN
                      AskPrice = ap }
                    |> fun td -> "Random", td
                    |> TradeData
                    |> actor.Tell)
        match wapts with
        | Some(wap, ts) -> 
            { CurrencyPair = "Random"
              TimeStamp = ts
              AskPrice = wap }
            |> WindowedTradeData
            |> expectMsg tk
            |> ignore
        | None -> expectNoMsg tk |> ignore
